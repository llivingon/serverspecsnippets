require 'spec_helper_aws'


RSpec.describe EC2::Instance.new('i-testinstance') do
  it { is_expected.to be_running }
  it { is_expected.to be_monitoring_enabled }
  it { is_expected.to be_on_windows }
  it { is_expected.to be_source_dest_checked }
  it { is_expected.to be_ebs_optimized }
  it { is_expected.to be_enhanced_networked }

  its(:image_id) { is_expected.to eq 'ami-aabbccdd' }
end
