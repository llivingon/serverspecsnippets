require 'spec_helper_exec'

describe file('c:/windows') do
  it { should be_directory }
  it { should_not be_writable.by('Everyone') }
 end

 describe service('DNS Client') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
 end 

describe host('www.google.com') do
  # ping
  it { should be_reachable }
  # tcp port 22
  it { should be_reachable.with( :port => 80 ) }
  # set protocol explicitly
  it { should be_reachable.with( :port => 80, :proto => 'tcp' ) }
end

describe interface('eth0') do
  it { should be_up }
end

describe package('requests') do
  it { should be_installed.by('pip') }
end
