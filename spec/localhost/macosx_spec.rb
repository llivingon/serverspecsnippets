require 'spec_helper_exec'

describe command('ls -al /') do
  its(:stdout) { should match /bin/ }
end

describe file('/tmp') do
  it { should be_directory }
end

describe host('www.google.com') do
  # ping
  it { should be_reachable }
  # tcp port 22
  it { should be_reachable.with( :port => 80 ) }
  # set protocol explicitly
  it { should be_reachable.with( :port => 80, :proto => 'tcp' ) }
end

describe interface('eth0') do
  it { should be_up }
end

describe package('requests') do
  it { should be_installed.by('pip') }
end

describe user('root') do
  it { should exist }
end
