require 'spec_helper'
#http://blog.teamtreehouse.com/an-introduction-to-rspec

class CurlRunner
  attr_reader :curlPath

  def initialize(curlPath={})
    @options = curlPath
  end

  def runCommand(curlCommand, recordResultToVariable=false)
    @result = `curl -k #{@options[:curlPath]}#{curlCommand} -H Accept:text/html -s -X GET`
    return @result
  end
 
end
 
curlPathVar = "http://www.dollar2rupee.net"

RSpec.describe CurlRunner do
  describe "Test dollar2rupee Using Curl" do
    subject { curlObj }
    curlRunnerObj = CurlRunner.new(:curlPath => curlPathVar)
	
 	context "Test home Page" do
      let(:curlObj) { curlRunnerObj.runCommand("/") }
      it { is_expected.to match("Current US Dollar to Rupee Exchange Rates") }
    end   
	context "Test history Page" do
      let(:curlObj) { curlRunnerObj.runCommand("/history") }
      it { is_expected.to match("US Dollar to Rupee Exchange Rates as on") }
    end
 
    context "Test chart Page" do
      let(:curlObj) { curlRunnerObj.runCommand("/chart") }
      it { is_expected.to match("US Dollar to Rupee Exchange Rates - Line Chart") }
    end

    context "Test Vote Page" do
      let(:curlObj) { curlRunnerObj.runCommand("/vote") }
      it { is_expected.to match("Vote for your favorite provider now") }
    end
  end
end