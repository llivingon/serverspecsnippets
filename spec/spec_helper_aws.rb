require 'aws-sdk'
require 'serverspec'

ENV['AWS_REGION']='eu-west-1'
ENV['aws_access_key_id']='akid'
ENV['aws_secret_access_key']='secret'

Aws.config[:ssl_verify_peer] = false
Aws.config[:logger]
Aws.config[:stub_responses] = false

#Update AWS Credentials
Aws.config.update(
  region: 'us-east-1',
  credentials: Aws::Credentials.new('akid', 'secret')
)

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.disable_monkey_patching!
  config.default_formatter = 'doc' if config.files_to_run.one?
  config.profile_examples = 5
  config.order = :random
  Kernel.srand config.seed
end

#Configuring libraries
require_relative '../lib/serverspec-aws'
include Serverspec::Type::AWS
set :backend, :exec

# Disabling Client stubbing
Aws.config[:stub_responses] = false