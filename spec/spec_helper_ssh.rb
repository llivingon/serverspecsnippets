require 'serverspec'
require 'net/ssh'
require 'base64'

set :backend, :ssh

host = ENV['TARGET_HOST']
sshKey = "base64-Encoded-SSHKeyString"

File.open('sshKey', 'w') { |file| file.write(Base64.decode64(sshKey)) }
  
ssh_key_file = 'sshKey'
options = Net::SSH::Config.for(host)

if ssh_key_file
  options[:keys] = [ssh_key_file]
end

options[:user] ||= "ssh-user"

#Configuring libraries
require_relative '../lib/serverspec-aws.rb'
include Serverspec::Type::AWS

set :host,        options[:host_name] || host
set :ssh_options, options
set :request_pty, true