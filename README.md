# README #

### What is this repository for? ###

* Serverspec (http://serverspec.org/) is a tool that applies a set of defined tests against a server instance. 
* With Serverspec, one can write RSpec tests for checking if **servers (or cloud infrastructure or something else)** are configured correctly. 
* Automation scripts test servers� actual state by executing command locally (on bash or windows command prompt) or via SSH.
* Version - 1.0

### How do I get Serverspec set up? ###
######Install Ruby######
~~~~
Refer - https://www.ruby-lang.org/en/documentation/installation/
~~~~
######Install below ruby gems######
~~~~
cmd>gem install serverspec
cmd>gem install bundle
~~~~

######Initialize serverspec######
~~~~
cmd>serverspec-init
Select OS type:
  1) UN*X
  2) Windows
Select number: 2
Select a backend type:
  1) WinRM
  2) Cmd (local)
Select number: 2
~~~~

######You should now see a folder structure as below######
```
+ spec/
+ spec/localhost/
+ spec/localhost/sample_spec.rb
+ spec/spec_helper.rb
+ Rakefile
+ Gemfile **You might have to create Gemfile if not created by default**
+ .rspec
```

######Now replace **sample_spec.rb** with below contents:######
```
require 'spec_helper'

 describe file('c:/windows') do
  it { should be_directory }
  it { should_not be_writable.by('Everyone') }
 end

 describe service('DNS Client') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
 end 
```

######Create a **Gemfile** with below contents:######
~~~~ 
source 'http://rubygems.org'

gem 'aws-sdk'
gem 'serverspec'
gem 'rake'
~~~~

######Finally, run below command to run tests######
~~~~
cmd>bundle exec rake spec 
~~~~

###### Tests results should look like below ######
~~~~
C:/Ruby22/bin/ruby.exe -I'C:/Ruby22/lib/ruby/gems/2.2.0/gems/rspec-core-3.6.0/lib';'C:/Ruby22/lib/ruby/gems/2.2.0/gems/rspec-support-3.
6.0/lib' 'C:/Ruby22/lib/ruby/gems/2.2.0/gems/rspec-core-3.6.0/exe/rspec' --pattern 'spec/localhost/*_spec.rb'

File "c:/windows"
  should be directory
  should not be writable

Service "DNS Client"
  should be installed
  should be enabled
  should be running

Finished in 10.25 seconds (files took 3.29 seconds to load)
5 examples, 0 failures
~~~~

###### If previouse steps didn't work, then follow next steps and rerun) ######
~~~~
cmd>bundle install
cmd>bundle update
cmd>bundle exec rake spec
</code>
~~~~

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* twitter.com/llivingon